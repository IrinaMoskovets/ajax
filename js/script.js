var form = document.querySelector('form'),
	wrap_form = document.querySelector('.wrap_form'),
	preload = document.querySelector('#load'),
	blockError = document.querySelector('#error'),
	profile = document.querySelector('#profile'),
	profileAvatar = document.querySelector('.avatar'),
	profileName = document.querySelector('.name'),
	profileCountry = document.querySelector('.country'),
	profileHobbies = document.querySelector('.hobbies');

/* функция для первого задания */
function loadProfile() {	
	var email = document.querySelector('#email').value,
		password = document.querySelector('#password').value,
		xhr = new XMLHttpRequest(),
		body = 'email=' + encodeURIComponent(email) + '&password=' + encodeURIComponent(password);
  
	xhr.open('POST', 'http://netology-hj-ajax.herokuapp.com/homework/login_json', true);
	xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	
	xhr.send(body);
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState != 4) return;				
		preload.style.display = "none";					
		var json = JSON.parse(xhr.responseText);
		
		if (xhr.status == 200) {
			profile.style.display = "block";
			form.style.display = "none";
			
			profileAvatar.src = json.userpic;
			profileName.innerHTML = 'Имя: ' + json.name + ' ' + json.lastname + '<br>';
			profileCountry.innerHTML = 'Страна: ' + json.country + '<br>';					
			profileHobbies.innerHTML = 'Список увлечений: ';
			json.hobbies.forEach(function (value, i) {
				if (i < json.hobbies.length-1) {
					profileHobbies.appendChild(document.createTextNode(value + ', '));
				} else {							
					profileHobbies.appendChild(document.createTextNode(value));
				}
			});
			
		} else {
			wrap_form.style.display = "inline-block";
			blockError.style.display = "block";		
			
			blockError.innerHTML = json.error.code + ' ' + json.error.message;
		}
	}	
	
	blockError.style.display = "none";
	wrap_form.style.display = "none";
	preload.style.display = "block";
}	
/* END функция для первого задания */

/* функция выхода из профиля */
function hideProfile() {
	profile.style.display = "none";
	form.style.display = "block";	
	wrap_form.style.display = "inline-block";			
	email.value = '';
	password.value = '';
	profileAvatar.src = '';
	profileName.innerHTML = '';
	profileCountry.innerHTML = '';					
	profileHobbies.innerHTML = '';
}
/* END функция выхода из профиля */

/* функция для второго задания */
function loadProfileXml() {	
	var email = document.querySelector('#email'),
		password = document.querySelector('#password'),
		xhr = new XMLHttpRequest(),
		body = JSON.stringify({
			email: email.value,
			password: password.value
		});
  
	xhr.open('POST', 'http://netology-hj-ajax.herokuapp.com/homework/login_xml', true);
	xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
	xhr.setRequestHeader('Content-Type', 'application/json');
	
	xhr.send(body);
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState != 4) return;				
		preload.style.display = "none";					
		var xml = xhr.responseXML;
						
		if (xhr.status == 200) {
			profile.style.display = "block";
			form.style.display = "none";
			
			var xmlAvatar = xml.querySelector('userpic').firstChild.nodeValue,
				xmlName = xml.querySelector('name').firstChild.nodeValue,
				xmlLastname = xml.querySelector('lastname').firstChild.nodeValue,
				xmlCountry = xml.querySelector('country').firstChild.nodeValue,
				xmlHobbies = xml.querySelectorAll('hobby');	
				
			profileAvatar.src = xmlAvatar;
			profileName.innerHTML = 'Имя: ' + xmlName + ' ' + xmlLastname + '<br>';
			profileCountry.innerHTML = 'Страна: ' + xmlCountry + '<br>';		
			profileHobbies.innerHTML = 'Список увлечений: ';
			
			xmlHobbies.forEach(function (value, i) {
				if (i < xmlHobbies.length-1) {
					profileHobbies.appendChild(document.createTextNode(value.firstChild.nodeValue + ', '));
				} else {							
					profileHobbies.appendChild(document.createTextNode(value.firstChild.nodeValue));
				}
			});
			
		} else {
			wrap_form.style.display = "inline-block";
			blockError.style.display = "block";		
			
			blockError.appendChild(xml.querySelector('error'));
		}
	}	
	
	blockError.style.display = "none";
	wrap_form.style.display = "none";
	preload.style.display = "block";
}	
/* END функция для второго задания */